/*
Copyright 2022 "zacryol"

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
use butlerswarm::*;

fn run_tool() -> Result<(), Box<dyn std::error::Error>> {
    let cli_arg_data = args_data::get();

    let command_arg = &cli_arg_data.command;
    let filename = &cli_arg_data.cfg_file_path;

    match command_arg.as_str() {
        "generate" => write_default_config(filename)?,
        _ => PushConfig::from_toml_file(filename)?.execute(&cli_arg_data.into())?,
    }
    Ok(())
}

fn main() {
    if let Err(e) = run_tool() {
        eprintln!("\x1b[91m{}\x1b[0m", e);
        std::process::exit(1)
    }
}

mod args_data {
    use butlerswarm::*;
    use clap::Parser;

    const COMMAND_FAIL_MSG: &str = r#"
COMMAND must be one of the following values:
"generate" - Write out default config file
"push" - Execute defined butler commands
"print" - Print out command text
"dry-run" - Execute commands with `--dry-run` flag"#;

    fn check_command(c: &str) -> Result<(), &'static str> {
        match c {
            "push" | "print" | "dry-run" | "generate" => Ok(()),
            _ => Err(COMMAND_FAIL_MSG),
        }
    }

    pub fn get() -> CLIArgData {
        CLIArgData::parse()
    }

    #[derive(Parser, Debug)]
    #[clap(version, about)]
    pub struct CLIArgData {
        /// 'push' to run, 'print' or 'dry-run' to test, 'generate' to write default
        #[clap(validator = check_command)]
        pub command: String,
        #[clap(short = 'f', long, default_value_t = String::from(".butlerswarm-cfg.toml"))]
        pub cfg_file_path: String,
        /// Specify location of butler (if not in PATH)
        #[clap(short, long)]
        butler_path: Option<String>,
        /// Subset of channels to run for
        #[clap(short = 's', long)]
        channel_set: Option<String>,
    }

    impl From<CLIArgData> for PushCommandParams {
        fn from(c: CLIArgData) -> Self {
            PushCommandParams {
                mode: {
                    match c.command.as_str() {
                        "push" => ExecuteMode::Deploy,
                        "print" => ExecuteMode::Print,
                        "dry-run" => ExecuteMode::DryRun,
                        _ => panic!("'{}' does not match any ExecuteMode", c.command),
                    }
                },
                butler_path: c.butler_path,
                channel_set: c.channel_set,
            }
        }
    }
}
