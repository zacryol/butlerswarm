/*
Copyright 2022 "zacryol"

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
use super::*;

#[test]
fn basic_toml_parsing() {
    assert!(PushConfig::from_toml_text("").is_err());
    {
        let pc = PushConfig::from_toml_text(
            r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        [channels.channel]"#,
        )
        .unwrap();
        assert_eq!(pc.itch_account_name, "my_name");
        assert_eq!(pc.itch_game, "my_game");
        assert_eq!(pc.base_dir, "build");
        assert_eq!(pc.channels.len(), 1);
        assert!(pc.channels.get("channel").is_some());
        assert_eq!(pc.butler_path, None);
        assert_eq!(pc.butler_ignores.len(), 0);
        assert_eq!(pc.version, Version::None);
        assert!(pc.channels["channel"].version.is_none());
    }
    {
        let pc = PushConfig::from_toml_text(
            r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        [channels.channel]
        [channels.channel2]"#,
        )
        .unwrap();
        assert_eq!(pc.itch_account_name, "my_name");
        assert_eq!(pc.itch_game, "my_game");
        assert_eq!(pc.base_dir, "build");
        assert_eq!(pc.channels.len(), 2);
        assert!(pc.channels.get("channel").is_some());
        assert!(pc.channels.get("channel2").is_some());
        assert_eq!(pc.butler_path, None);
        assert_eq!(pc.butler_ignores.len(), 0);
        assert_eq!(pc.version, Version::None);
        assert!(pc.channels["channel"].version.is_none());
        assert!(pc.channels["channel2"].version.is_none());
    }

    assert!(PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build""#
    )
    .is_err());

    assert!(PushConfig::from_toml_text(
        r#"
    itch_account_name = true
    itch_game = "my_game"
    base_dir = "build"
    [channels.channel]"#
    )
    .is_err());

    assert!(PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = 5
    base_dir = "build"
    [channels.channel]"#
    )
    .is_err());
}

#[test]
fn more_toml_parsing() {
    {
        let pc = PushConfig::from_toml_text(
            r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        butler_path = "~/bin/butler"
        butler_ignores = ["*.txt"]
        [channels.channel]"#,
        )
        .unwrap();
        assert_eq!(pc.butler_ignores.len(), 1);
        assert!(pc.butler_path.is_some());
        assert_eq!(pc.butler_ignores.len(), 1);
        assert_eq!(pc.butler_path.unwrap(), "~/bin/butler");
        assert_eq!(pc.channels.len(), 1);
        let c = &pc.channels["channel"];
        assert!(c.dir.is_none());
        assert_eq!(c.butler_ignores.len(), 0);
        assert!(c.version.is_none());
    }
    {
        let pc = PushConfig::from_toml_text(
            r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        butler_path = "~/bin/butler"
        butler_ignores = ["*.txt"]
        [channels.channel]
        name = "channel"
        dir.path = "channel_dir"
        butler_ignores = ["*.dll", "*.json"]"#,
        )
        .unwrap();
        assert!(pc.butler_path.is_some());
        assert_eq!(pc.butler_ignores.len(), 1);
        assert_eq!(pc.butler_path.unwrap(), "~/bin/butler");
        assert_eq!(pc.channels.len(), 1);
        let c = &pc.channels["channel"];
        assert_eq!(
            *c.dir.as_ref().unwrap(),
            ChannelDir::Rel {
                path: String::from("channel_dir")
            }
        );
        assert_eq!(c.butler_ignores.len(), 2);
    }
    {
        let pc = PushConfig::from_toml_text(
            r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        [channels.channel]
        dir.full_path = "channel_dir""#,
        )
        .unwrap();
        assert!(pc.butler_path.is_none());
        assert_eq!(pc.butler_ignores.len(), 0);
        assert_eq!(pc.channels.len(), 1);
        let c = &pc.channels["channel"];
        assert_eq!(
            *c.dir.as_ref().unwrap(),
            ChannelDir::Full {
                full_path: String::from("channel_dir")
            }
        );
    }

    // defines both rel and full paths, should error
    assert!(PushConfig::from_toml_text(
        r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        [channels.channel]
        dir.path = "channel"
        dir.full_path = "channel_dir""#
    )
    .is_err());
}

#[test]
fn version_parsing() {
    {
        let pc = PushConfig::from_toml_text(
            r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        [channels.channel]
        name = "channel""#,
        )
        .unwrap();
        assert_eq!(pc.version, Version::None);
        assert_eq!(pc.channels["channel"].version, None);
    }
    {
        let pc = PushConfig::from_toml_text(
            r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        version.type = "none"
        [channels.channel]"#,
        )
        .unwrap();
        assert_eq!(pc.version, Version::None);
        assert_eq!(pc.channels["channel"].version, None);
    }
    {
        let pc = PushConfig::from_toml_text(
            r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        version.type = "string"
        version.data = "1.0"
        [channels.channel]
        version.type = "none"
        [channels.channel2]
        version.type = "file"
        version.data = "version.txt""#,
        )
        .unwrap();
        assert_eq!(pc.version, Version::String(String::from("1.0")));
        assert_eq!(pc.channels["channel"].version, Some(Version::None));
        assert_eq!(
            pc.channels["channel2"].version,
            Some(Version::File(String::from("version.txt")))
        );
    }
    // provides version type that requires data with data missing
    assert!(PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build"
    version.type = "string"
    [channels.channel]"#
    )
    .is_err());

    // provides version data to type that omits it
    assert!(PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build"
    version.type = "none"
    version.data = true
    [channels.channel]"#
    )
    .is_err());

    // provide data without type
    assert!(PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build"
    version.data = "1.0"
    [channels.channel]"#
    )
    .is_err());
    assert!(PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build"
    version.type = "string"
    version.data = "1.0"
    [channels.channel]
    version.data = "2.0""#
    )
    .is_err());

    assert_eq!(Version::default(), Version::None);
}

#[test]
fn is_default_toml_valid() {
    // Valid as is
    assert!(
        PushConfig::from_toml_text(include_str!("default.toml")).is_ok(),
        "Default failed"
    );
    // Valid with single-'#'-commented lines decommented
    assert!(
        PushConfig::from_toml_text({
            let base = include_str!("default.toml");
            &base
                .split_inclusive('\n')
                .map(|line| {
                    if let Some(l) = line.strip_prefix('#') {
                        l
                    } else {
                        line
                    }
                })
                .collect::<String>()
        })
        .is_ok(),
        "Uncommented default failed"
    );
}

#[test]
fn command_building_basic() {
    {
        let pc = PushConfig::from_toml_text(include_str!("default.toml")).unwrap();
        assert_eq!(
            format!(
                "{:?}",
                pc.get_butler_command(
                    (&String::from("windows"), &pc.channels["windows"]),
                    &PushCommandParams {
                        mode: Print,
                        butler_path: None,
                        channel_set: None
                    }
                )
                .unwrap()
            ),
            r#""butler" "push" "build/windows" "my-cool-name/my-cool-game:windows""#
        );
        assert_eq!(
            format!(
                "{:?}",
                pc.get_butler_command(
                    (&String::from("linux"), &pc.channels["linux"]),
                    &PushCommandParams {
                        mode: Print,
                        butler_path: None,
                        channel_set: None
                    }
                )
                .unwrap()
            ),
            r#""butler" "push" "build/linux" "my-cool-name/my-cool-game:linux""#
        );
        assert_eq!(
            format!(
                "{:?}",
                pc.get_butler_command(
                    (&String::from("web"), &pc.channels["web"]),
                    &PushCommandParams {
                        mode: Print,
                        butler_path: None,
                        channel_set: None
                    }
                )
                .unwrap()
            ),
            r#""butler" "push" "build/web" "my-cool-name/my-cool-game:web""#
        );
        assert_eq!(
            format!(
                "{:?}",
                pc.get_butler_command(
                    (&String::from("windows"), &pc.channels["windows"]),
                    &PushCommandParams {
                        mode: DryRun,
                        butler_path: None,
                        channel_set: None
                    }
                )
                .unwrap()
            ),
            r#""butler" "push" "build/windows" "my-cool-name/my-cool-game:windows" "--dry-run""#
        );
        assert_eq!(
            format!(
                "{:?}",
                pc.get_butler_command(
                    (&String::from("linux"), &pc.channels["linux"]),
                    &PushCommandParams {
                        mode: DryRun,
                        butler_path: None,
                        channel_set: None
                    }
                )
                .unwrap()
            ),
            r#""butler" "push" "build/linux" "my-cool-name/my-cool-game:linux" "--dry-run""#
        );
        assert_eq!(
            format!(
                "{:?}",
                pc.get_butler_command(
                    (&String::from("web"), &pc.channels["web"]),
                    &PushCommandParams {
                        mode: DryRun,
                        butler_path: None,
                        channel_set: None
                    }
                )
                .unwrap()
            ),
            r#""butler" "push" "build/web" "my-cool-name/my-cool-game:web" "--dry-run""#
        );
    }
    let pc = PushConfig::from_toml_text(
        r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        butler_path = "~/bin/butler"
        butler_ignores = ["*.txt"]
        [channels.channel]"#,
    )
    .unwrap();
    assert_eq!(
        format!(
            "{:?}",
            pc.get_butler_command(
                (&String::from("channel"), &pc.channels["channel"]),
                &PushCommandParams {
                    mode: Print,
                    butler_path: None,
                    channel_set: None
                }
            )
            .unwrap()
        ),
        r#""~/bin/butler" "push" "--ignore" "*.txt" "build/channel" "my_name/my_game:channel""#
    );
    /* Order of `--ignore` flags no longer guaranteed, remove this test for now
    let pc = PushConfig::from_toml_text(
        r#"
        itch_account_name = "my_name"
        itch_game = "my_game"
        base_dir = "build"
        butler_path = "~/bin/butler"
        butler_ignores = ["*.txt"]
        [channels.channel]
        butler_ignores = ["*.log"]"#,
    )
    .unwrap();
    assert_eq!(
        format!(
            "{:?}",
            pc.get_butler_command(
                (&String::from("channel"), &pc.channels["channel"]),
                &PushCommandParams {
                    mode: Print,
                    butler_path: None,
                    channel_set: None
                }
            )
            .unwrap()
        ),
        r#""~/bin/butler" "push" "--ignore" "*.txt" "--ignore" "*.log" "build/channel" "my_name/my_game:channel""#
    );
    */
}

#[test]
fn no_allow_mult_channels() {
    // case sensitive repitition is detected by toml parser
    match PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build"
    [channels.channel]
    [channels.channel]"#,
    ) {
        Err(PushConfigErr::TOMLParse(_)) => (),
        Err(e) => panic!("Did not return toml parse Err; {} instead", e),
        _ => panic!("Did not return Err"),
    };

    // case insensitive is detected in validate()
    match PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build"
    [channels.channel]
    [channels.CHANNEL]
    name = "CHANNEL""#,
    ) {
        Err(PushConfigErr::MultChannelDef(_)) => (),
        Err(e) => panic!("Did not return MultChannelDef Err; {} instead", e),
        _ => panic!("Did not return Err"),
    };
}

#[test]
fn no_allow_fake_channels() {
    match PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build"
    [channels.real]
    [channelsets.set]
    channels = ["fake", "real"]"#,
    ) {
        Err(PushConfigErr::ChSetNonExCh(h)) => {
            assert_eq!(h.len(), 1);
            assert!(h.get("set").is_some());
            assert_eq!(h.get("set").unwrap().len(), 1);
        }
        Err(e) => panic!("Did not return ChSetNonExCh Err; {} instead", e),
        _ => panic!("Did not return ChSetNonExCh Err"),
    }
    // channel names are currently compared case-sensitive for nonexistence
    match PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build"
    [channels.real]
    [channelsets.set]
    channels = ["fake", "REAL"]"#,
    ) {
        Err(PushConfigErr::ChSetNonExCh(h)) => {
            assert_eq!(h.len(), 1);
            assert!(h.get("set").is_some());
            assert_eq!(h.get("set").unwrap().len(), 2);
        }
        Err(e) => panic!("Did not return ChSetNonExCh Err; {} instead", e),
        _ => panic!("Did not return ChSetNonExCh Err"),
    }

    match PushConfig::from_toml_text(
        r#"
    itch_account_name = "my_name"
    itch_game = "my_game"
    base_dir = "build"
    [channels.real]
    [channelsets.set]
    channels = ["fake", "REAL"]
    [channelsets.set2]
    channels = ["fake", "fake2"]"#,
    ) {
        Err(PushConfigErr::ChSetNonExCh(h)) => {
            assert_eq!(h.len(), 2);
            assert!(h.get("set").is_some());
            assert_eq!(h.get("set").unwrap().len(), 2);
            assert!(h.get("set2").is_some());
            assert_eq!(h.get("set2").unwrap().len(), 2);
        }
        Err(e) => panic!("Did not return ChSetNonExCh Err; {} instead", e),
        _ => panic!("Did not return ChSetNonExCh Err"),
    }
}

#[test]
fn invalid_channel_set_error() {
    let pc = PushConfig {
        itch_account_name: String::from("my_name"),
        itch_game: String::from("my_game"),
        base_dir: String::from("build"),
        butler_ignores: Default::default(),
        butler_path: None,
        version: Version::default(),
        channels: Default::default(),
        channelsets: HashMap::default(),
    };
    match pc.execute(&PushCommandParams {
        mode: Print,
        butler_path: None,
        channel_set: Some(String::from("not_real")),
    }) {
        Err(e) if e.to_string() == "channel set 'not_real' was not found in config" => (),
        _ => panic!("Did not return InvalidChSetError(\"not_real\")."),
    };
}

#[test]
#[should_panic(expected = "Version::None does not apply Command args.")]
fn version_none_arg_string() {
    Version::None.get_arg_string();
}
