/*
Copyright 2022 "zacryol"

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

//! butlerswarm
//!
//! `butlerswarm` is a helper for itch.io's [butler](https://itchio.itch.io/butler) cli tool,
//! enabling the running of multiple commands at once

use serde_derive::Deserialize;
use std::collections::{HashMap, HashSet};
use std::process::Command;
use std::str::FromStr;
use std::{fs, io};
use ExecuteMode::*;

pub mod error;
#[cfg(test)]
mod tests;

use error::InvalidChSetError;
pub use error::PushConfigErr;

type FullChannelR<'a> = (&'a String, &'a ChannelConfig);
type ChIter<'a> = Box<dyn Iterator<Item = FullChannelR<'a>> + 'a>;

/// Write out the default config file.
///
/// The default is not usable directly, as user information must still be provided.
/// Rather, it is more to set up a template that allows a user to more easily fill in the information
/// needed without needing to memorize each key.
/// # Errors
/// The method simply forwards the return value of [fs::write], so an Err will be returned if that operation fails.
pub fn write_default_config(outfile: &str) -> io::Result<()> {
    fs::write(
        outfile,
        format!(include_str!("default.toml"), env!("CARGO_PKG_VERSION")),
    )
}

/// Encodes data to generate and execute `butler` commands.
/// # Examples
/// ```
/// use butlerswarm::PushConfig;
/// use butlerswarm::PushCommandParams;
/// use butlerswarm::ExecuteMode::*;
///
/// // Valid, not recommended.
/// let pc: PushConfig = toml::toml! {
///     itch_account_name = "my-cool-name"
///     itch_game = "my-cool-game"
///     base_dir = "build"
///
///     [channels.windows]
///
///     [channels.linux]
///
///     [channels.web]
/// }.try_into().unwrap();
///
///
/// // Preferred method, assuming that config file exists, and is valid.
/// // let pc = PushConfig::from_toml_file(".butlerswarm-cfg.toml").unwrap();
/// # let pc = PushConfig::from_toml_text(include_str!("default.toml")).unwrap(); // so file does not need to be present for cargo test
///
/// let params = PushCommandParams { mode: Deploy, butler_path: None, channel_set: None };
/// # let params = PushCommandParams { mode: Print, butler_path: None, channel_set: None }; // Prevent from actually running in cargo test
///
/// pc.execute(&params).unwrap();
/// ```
/// With this Rust code, the following `butler` commands will be executed:
/// ```sh
/// butler push build/windows my-cool-name/my-cool-game:windows
/// butler push build/linux my-cool-name/my-cool-game:linux
/// butler push build/web my-cool-name/my-cool-game:web
/// ```
/// More detailed TOML specification for `butlerswarm` can be found [here](https://gitlab.com/zacryol/butlerswarm/-/blob/v0.9.1/TOML_SPEC.md)
#[derive(Deserialize)]
pub struct PushConfig {
    itch_account_name: String,
    itch_game: String,
    base_dir: String,
    channels: HashMap<String, ChannelConfig>,
    butler_path: Option<String>,
    #[serde(default)]
    butler_ignores: HashSet<String>,
    #[serde(default)]
    channelsets: HashMap<String, ChannelSet>,
    #[serde(default)]
    version: Version,
}

impl FromStr for PushConfig {
    type Err = PushConfigErr;

    /// Interprets [&str] as TOML text for parsing.
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::from_toml_text(s)
    }
}

impl TryFrom<&str> for PushConfig {
    type Error = <Self as FromStr>::Err;

    /// Interprets [&str] as TOML text for parsing.
    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::from_toml_text(s)
    }
}

impl PushConfig {
    /// Generates PushConfig from the TOML-formatted text `from`.
    /// # Errors
    /// Will return an Err if the TOML is invalid.
    pub fn from_toml_text(from: &str) -> Result<Self, PushConfigErr> {
        toml::from_str::<PushConfig>(from)?.validate()
    }

    /// Reads the contents of the file at path `file` and uses that to construct a PushConfig.
    /// # Errors
    /// Will return an Err if the file can not be read, or if the TOML is invalid.
    pub fn from_toml_file(file: &str) -> Result<Self, PushConfigErr> {
        Self::from_toml_text(&fs::read_to_string(file)?)
    }

    // Ensures validity of PushConfig
    // Even if TOML parses successfully, something else might be wrong
    fn validate(self) -> Result<Self, PushConfigErr> {
        // Check for multiple channel definitions (case insensitive)
        let dupes: HashSet<String> = {
            let names_lower = self
                .channels
                .keys()
                .map(|c| c.to_lowercase())
                .collect::<Vec<String>>();
            names_lower
                .iter()
                .collect::<HashSet<&String>>()
                .into_iter()
                .filter(|c| names_lower.iter().filter(|&c2| *c == c2).count() > 1)
                .cloned()
                .collect()
        };
        if !dupes.is_empty() {
            return Err(PushConfigErr::MultChannelDef(dupes));
        }
        // Check for non existent channels in sets
        let nonex: HashMap<_, _> = self
            .channelsets
            .iter()
            .map(|set| {
                (set.0, {
                    set.1
                        .channels
                        .iter()
                        .filter(|c| !self.channels.keys().any(|c2| c2 == *c))
                        .collect::<Vec<_>>()
                })
            })
            .filter(|t| !t.1.is_empty())
            .map(|(s, v)| (s.clone(), v.into_iter().cloned().collect::<Vec<_>>()))
            .collect();
        if !nonex.is_empty() {
            return Err(PushConfigErr::ChSetNonExCh(nonex));
        }

        Ok(self)
    }

    fn get_set_by_name(&self, name: &str) -> Result<&ChannelSet, InvalidChSetError> {
        self.channelsets
            .get(name)
            .ok_or_else(|| InvalidChSetError::from(name.to_string()))
    }

    fn get_ch_iter(&self, pcp: &PushCommandParams) -> Result<ChIter, InvalidChSetError> {
        Ok(pcp // &PushCommandParams
            .channel_set // &Option<String>
            .as_ref() // Option<&String>
            .map(|s| self.get_set_by_name(s)) // Option<Result<&ChannelSet, InvalidChSetError>>
            .transpose()? // Result<Option<&ChannelSet>, InvalidChSetError> => Option<&ChannelSet>
            .map_or_else(
                || Box::new(self.channels.iter()) as ChIter,
                |set| Box::new(self.channels.iter().filter(|ch| set.has_channel(ch.0))) as ChIter,
            )) // Ok(ChIter)
    }

    /// Builds and uses a [Command] for each channel as defined in the config, in accordance with the [PushCommandParams].
    /// # Errors
    /// Will return an [InvalidChSetError] if the [PushCommandParams] provides a channel set name that is not found.
    pub fn execute(&self, pcp: &PushCommandParams) -> Result<(), InvalidChSetError> {
        let ch_iter = self.get_ch_iter(pcp)?;
        for channel in ch_iter {
            let mut c = match self.get_butler_command(channel, pcp) {
                Ok(c) => c,
                Err(e) => {
                    eprintln!("\x1b[91merror '{}'\non channel '{}'\x1b[0m", e, channel.0);
                    continue;
                }
            };
            match &pcp.mode {
                Deploy => {
                    let _r = c.status();
                }
                DryRun => {
                    println!("\n\x1b[94;4m==== For channel '{}' ====\x1b[0m", channel.0);
                    let _r = c.status();
                }
                Print => {
                    println!("{:?}", c);
                }
            };
        }
        Ok(())
    }

    fn get_butler_command(
        &self,
        channel: FullChannelR,
        pcp: &PushCommandParams,
    ) -> Result<Command, error::CommandGenErr> {
        let mut command = Command::new(
            // Pick path to use: priority pcp value > self value > default
            pcp.butler_path
                .as_ref()
                .or(self.butler_path.as_ref())
                .map(|s| s.as_str())
                .unwrap_or(Self::BUTLER_STRING_DEF),
        );
        command.arg("push");

        // Apply global and channel `--ignore` args
        [&self.butler_ignores, &channel.1.butler_ignores]
            .into_iter()
            .flatten()
            .collect::<HashSet<_>>()
            .into_iter()
            .for_each(|i| {
                command.arg("--ignore").arg(i);
            });

        // Directory to push
        command.arg({
            let b_d = self.base_dir.trim_end_matches('/');
            match &channel.1.dir {
                None => format!("{}/{}", b_d, channel.0),
                Some(ChannelDir::Full { full_path }) => full_path.to_string(),
                Some(ChannelDir::Rel { path }) => format!("{}/{}", b_d, path),
            }
        });

        // Game ID data
        command.arg(format!(
            "{}/{}:{}",
            self.itch_account_name, self.itch_game, channel.0
        ));

        // Version
        channel
            .1
            .version
            .as_ref()
            .unwrap_or(&self.version)
            .apply_command_args(&mut command)?;

        if pcp.mode == ExecuteMode::DryRun {
            command.arg("--dry-run");
        }

        Ok(command)
    }

    // default butler string path
    // butler.exe on Windows, butler everything else
    #[cfg(target_os = "linux")]
    const BUTLER_STRING_DEF: &'static str = "butler";

    #[cfg(target_os = "windows")]
    const BUTLER_STRING_DEF: &'static str = "butler.exe";

    #[cfg(target_os = "macos")]
    const BUTLER_STRING_DEF: &'static str = "butler";
}

#[derive(Deserialize)]
#[serde(untagged)]
#[serde(deny_unknown_fields)]
#[cfg_attr(test, derive(Debug, PartialEq))]
enum ChannelDir {
    Full { full_path: String },
    Rel { path: String },
}

#[derive(Deserialize)]
struct ChannelConfig {
    dir: Option<ChannelDir>,
    #[serde(default)]
    butler_ignores: HashSet<String>,
    version: Option<Version>,
}

#[derive(Deserialize)]
struct ChannelSet {
    channels: Vec<String>,
}

impl ChannelSet {
    fn has_channel(&self, ch_name: &str) -> bool {
        self.channels.iter().any(|n| n == ch_name)
    }
}

/// Version data for the build
#[derive(Deserialize)]
#[serde(tag = "type", content = "data")]
#[cfg_attr(test, derive(Debug, PartialEq))]
#[serde(rename_all = "snake_case")]
pub enum Version {
    /// use `--userversion`
    String(String),
    /// use `--userversion-file`
    File(String),
    /// use the current git commit with `--userversion`
    Git,
    /// no version data, let itch take care of it
    None,
}

impl Version {
    const V: &'static str = "--userversion";
    const VF: &'static str = "--userversion-file";

    fn get_arg_string(&self) -> &'static str {
        match self {
            Self::String(_) | Self::Git => Self::V,
            Self::File(_) => Self::VF,
            Self::None => panic!("Version::None does not apply Command args."),
        }
    }

    fn apply_command_args(&self, command: &mut Command) -> Result<(), error::CommandGenErr> {
        match self {
            Self::String(s) | Self::File(s) => command.arg(self.get_arg_string()).arg(s),
            Self::Git => command.arg(self.get_arg_string()).arg({
                let mut c = Command::new("git");
                c.arg("rev-parse").arg("--short").arg("HEAD");
                String::from_utf8({
                    let o = c.output()?;
                    if !o.status.success() {
                        return Err(error::CommandGenErr::from(o));
                    }
                    o.stdout
                })?
                .trim_end()
            }),
            Self::None => command,
        };
        Ok(())
    }
}

impl Default for Version {
    fn default() -> Self {
        Version::None // makes 'version.type = "None"' optional on global config
    }
}

/// Data for building Commands not read in from file
pub struct PushCommandParams {
    /// Defines how the PushConfig will deal with the command data.
    pub mode: ExecuteMode,
    /// If provided, will specify location of `butler` executable.
    pub butler_path: Option<String>,
    /// Which of the [PushConfig]'s channel sets to use.
    /// If not provided, all channels will be used.
    pub channel_set: Option<String>,
}

/// Define how [PushConfig::execute] will use generated [Command]s.
#[derive(PartialEq)]
pub enum ExecuteMode {
    /// Invoke `butler` commands with the `--dry-run` flag.
    DryRun,
    /// Run the `butler` commands, pushing builds to [itch.io](http://itch.io).
    Deploy,
    /// Print the text of the commands via [Command]'s [core::fmt::Debug] implementation.
    Print,
}
