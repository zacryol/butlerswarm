# Spec

Specification for `.butlerswarm-cfg.toml` files.

## Global keys:

- `itch_account_name`

    Required. The name of the account the project is under.

- `itch_game`

    Required. The project to push a build to.

- `base_dir`

    Required. This is the path to the directory (either absolute path or relative from cwd) that contains the directories that will be pushed.

- `butler_path`

    Optional. Full path to `butler` executable. If not provided, will simply use `butler` (or `butler.exe` on Windows) which must be in your `PATH`.
    
    **Partially deprecated.** The `--butler-path` (`-b`) command-line arg, which overrides this value, should be preferred, especially for shared projects.

- `butler_ignores`

    Optional. Array of Strings to each be passed to `butler` through an `--ignore` flag. See butler docs [Appendix C](https://itch.io/docs/butler/pushing.html#appendix-c-ignoring-files) for more info.

    **Note**: as of `butlerswarm` version `0.9.1`, the order of `--ignore` flags is not guaranteed.

### Examples:

```toml
itch_account_name = "me"
itch_game = "my_game"
base_dir = "build"
```

```toml
itch_account_name = "me"
itch_game = "my_game"
base_dir = "export"
butler_ignores = ["*.log"]
```

## Channels

After the main keys, a `channels` table must be defined for each channel that should be pushed to, in the format of `[channels.channelname]`, (each channel name being unique,) with the following keys:

- `butler_ignores`

    Optional. Array of Strings to each be passed to `butler` through an `--ignore` flag, *in addition to* those specified in the global `butler_ignores` value.

- `dir`

    Optional. The directory to push. Can be provided in one of two variants:
    - `dir.path` - Provides a path relative to the main config's `base_dir` value.
    - `dir.full_path` - Provides a full path, either absolute or relative to cwd, ignoring the global `base_dir` value.
    
    If no `dir` is provided, the channel name will be used as a `dir.path`

**Note**: While the toml keys are case sensitive, the channel names as handled by the itch.io backend are *not*.

### Examples:

```toml
[channels.linux]
```

```toml
[channels.linux]
dir.path = "linux" # has no effect
butler_ignores = ["*.so"]
```
```toml
[channels.web]
dir.path = "html5"
```

```toml
[channels.windows-nightly]
dir.full_path = "nightly-builds/windows"
```

```toml
# INVALID - WILL ERROR
[channels.windows]
dir.path = "win"
dir.full_path = "buils/windows"
```

```toml
# not valid
[channels.web]
[channels.web]
```

```toml
# also not valid
[channels.web]
[channels.WEB]
```

## Version data

Version data can be added globally or per channel, using a `version` subtable, with the keys `type`, and (possibly) `data`. `version.type` is the type of version data that is provided to `butler`, and `version.data` is used to provide additional information if needed.

The possible values for `version.type` are:

- `"string"`: pass in a value with `--userversion`. `version.data` should be the version string to use.
- `"file"`: pass in a value with `--userversion-file`. `version.data` should be the path to the file to read the version string from.
- `"git"`: use the currently checked out git commit as the version string. This string is acquired via executing `git rev-parse --short HEAD`. `version.data` should be omitted.
- `"none"`: do not pass in version data. The itch.io backend will assign a version number automatically. `version.data` should be omitted.

**Note:** `version.data` cannot be added by itself to a `[channels]` table, even if the `version.type` is the same.
If a channel is to use a different version than the globally defined one, `version.type` *must* be defined.

### Examples:

```toml
# Example defined for either global or channel version data
version.type = "file"
version.data = "version.txt"
```
Will use `--userversion-file version.txt`

```toml
# Example defined for global version data
[version]
type = "string"
data = "1.0"
```
Will use `--userversion 1.0`

```toml
# Example defined for channel, following channel definition
[channels.channelname.version]
type = "git"
```
Will use something like `--userversion 180e224`

## Channel Sets

Any number of `channelsets` subtables may be defined as `[channelsets.set_name]` to specify a group of channels, with the following key:

- `channels`: an Array of Strings representing the names of the channels to push as a part of that set. Each channel name must correspond to the name of a `[channels]` table.

### Example:

```toml
[channels.windows]
[channels.windows-nightly]
[channels.linux]
[channels.linux-nightly]

[channelsets.nightly]
channels = ["windows-nightly", "linux-nightly"]

[channelsets.stable]
channels = ["windows", "linux"]
```

With a `.butlerswarm-cfg.toml` file containing these channel sets, `butlerswarm push -s nightly` can be run to only push the two nightly channels.

## Example:

```toml
itch_account_name = "my-cool-name"
itch_game = "my-cool-game"
version.type = "string"
version.data = "1.0"
base_dir = "build"

[channels.web]
version.type = "None"

[channels.windows]

[channels.linux]

[channels.windows-32bit]
dir.path = "win32"

[channels.linux-32bit]
dir.path = "lin32"

[channels.windows-beta]
dir.full_path = "beta_builds/windows"
version.type = "file"
version.data = "version_beta.txt"

[channels.linux-beta]
dir.full_path = "beta_builds/linux"
version.type = "file"
version.data = "version_beta.txt"
```

with this `.butlerswarm-cfg.toml` file, the following commands will be executed upon running `butlerswarm push`:

```
butler push build/web my-cool-name/my-cool-game:web
butler push build/windows my-cool-name/my-cool-game:windows --userversion 1.0
butler push build/linux my-cool-name/my-cool-game:linux --userversion 1.0
butler push build/win32 my-cool-name/my-cool-game:windows-32bit --userversion 1.0
butler push build/lin32 my-cool-name/my-cool-game:linux-32bit --userversion 1.0
butler push beta_builds/windows my-cool-name/my-cool-game:windows-beta --userversion-file version_beta.txt
butler push beta_builds/linux my-cool-name/my-cool-game:linux-beta --userversion-file version_beta.txt
```
