# butlerswarm

Wrapper program to help manage multiple builds with itch.io's [butler](https://itchio.itch.io/butler) tool.

## Usage

`butlerswarm generate` - Write out a template config file.

`butlerswarm push` - Execute the `butler` commands as defined in the toml to upload.

`butlerswarm print` - Print out the command text (for testing the toml).

`butlerswarm dry-run` - Execute `butler` commands with the `--dry-run` flag, showing what files will be pushed.

The progam reads from a `.butlerswarm-cfg.toml` file, formatted like:

```toml
itch_account_name = "my-cool-name"
itch_game = "my-cool-game"
base_dir = "build"

[channels.windows]

[channels.linux]
```

The username, project name, channels and directories are used to assemble and execute `butler` commands.

The example shown will run the commands upon executing `butlerswarm push`:

```
butler push build/windows my-cool-name/my-cool-game:windows
butler push build/linux my-cool-name/my-cool-game:linux
```

For more detail, see [TOML_SPEC.md](TOML_SPEC.md)

## Requirements

`butler` (or `butler.exe` for windows) must be present in your `PATH` (or at the location otherwise specified), and authenticated to the desired itch account, either via running `butler login` or the `BUTLER_API_KEY` environment.

So far, this program has only been tested on Linux, but should in theory run on Windows, OSX.

## License

Licensed under the Apache License, Version 2.0 (the "License"), or the MIT license;

See [LICENSE](LICENSE) or [LICENSE-MIT](LICENSE-MIT).
